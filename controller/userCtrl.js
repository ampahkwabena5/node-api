const UserSchema = require('../models/user_model');

exports.getUsers = (req, res) => {
    let users = UserSchema.find((err, result) => {
        if (err) {
            res.status(500);
            res.json({
                responseCode: '500',
                message: err,
                data: null
            });
        }

        res.status(200);
        res.json({
            responseCode: '200',
            message: 'avalable users',
            data: result
        });
    });

}

exports.deleteUser = (req, res) => {
    let id = req.params.id
    if (id == undefined) {
        res.status(404);
        res.json({
            responseCode: '404',
            message: 'Invalid parameter',
            data: null
        });

    }

    res.status(200);
    res.json({
        responseCode: '200',
        message: 'user deleted successfully' + id,
        data: null
    });
}

exports.updateUser = (req, res) => {
    res.status(201);
    res.json({
        responseCode: '200',
        message: 'user updated successfully',
        data: null
    });
}

exports.saveUsers = (req, res) => {
    let user = new UserSchema({
        name: req.body.name,
        age: req.body.age,
        gender: req.body.gender,
        location: req.body.location,
    });
    user.save();
    res.status(201);
    res.json({
        responseCode: '201',
        message: 'user created successfully',
        data: null
    });
}


exports.getOneUser = (req, res) => {
    let id = req.params.id
    if (id == undefined) {
        res.status(404);
        res.json({
            responseCode: '404',
            message: 'User Not Found',
            data: null
        });

    }
    // let _id = req.params.id
    UserSchema.findOne({
        _id: id
    }, (err, result) => {
        if (err) {
            res.status(500);
            res.json({
                responseCode: '500',
                message: 'user not found',
                data: null
            });
        }
        res.status(200);
        res.json({
            responseCode: '200',
            message: 'Succefully found',
            data: result
        });
    })


}