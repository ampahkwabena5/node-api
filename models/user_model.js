const mongoose = require('mongoose');
const UserSchema = mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  age: {
    type: Number,
    require: true,
  },
  gender: {
    type: String,
    require: true,
  },
  location: {
    type: String,
    require: true,
  },
});

module.exports = mongoose.model('UserSchema', UserSchema);