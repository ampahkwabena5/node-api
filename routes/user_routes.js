const express = require('express');
const userController = require('../controller/userCtrl')
const router = express.Router();

router.get('/users', userController.getUsers);

router.post('/create-user', userController.saveUsers);

router.delete('/delete-user/:id', userController.deleteUser);

router.get('/get-user/:id', userController.getOneUser);

module.exports = router;