const express = require('express');
const mongoose = require('mongoose');

const router = require('./routes/user_routes');
const bodyParser = require("body-parser") // new

const port = 3000;

var mongoDB = 'mongodb://localhost:27017/users';

mongoose
  .connect(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true,
  })
  .then(() => {
    const app = express()
    app.use(express.json()) // new
    app.use(bodyParser.json()) // new
    app.use("/api", router)

    app.listen(port, () =>
      console.log(`Example app listening on port port! ${port}`)
    );
  })
  .catch((err) => {
    console.log(err);
  });